import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import { Layout, Input, Table } from "antd";
const { Header, Content, Footer } = Layout;
const { Search } = Input;

const columns = [
  {
    title: "Categories",
    dataIndex: "",
    key: "",
  },
];

const App = () => {
  const [dataListAll, setDataListAll] = useState([]);
  const [dataFilter, setDataFilter] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    fecthData();
  }, []);

  useEffect(() => {
    setDataFilter(
      dataListAll.filter((value) =>
        value.toLowerCase().includes(search.toLowerCase())
      )
    );
  }, [search, dataListAll]);

  const fecthData = () => {
    axios
      .get("https://api.publicapis.org/categories")
      .then((response) => setDataListAll(response.data));
  };

  console.log("dataListAll", dataListAll);
  const onSearch = (value) => setSearch(value);

  return (
    <div className="App">
      <Layout className="layout">
        <Header>
          <h1 style={{ color: "white" }}>Categories</h1>
        </Header>
        <Content style={{ padding: "0 50px" }}>
          <div className="site-layout-content">
            <div>
              <Search
                placeholder="input search text"
                onSearch={onSearch}
                style={{ width: 200 }}
              />
            </div>
            <Table dataSource={dataFilter} columns={columns} />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </div>
  );
};

export default App;
